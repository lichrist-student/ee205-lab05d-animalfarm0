///////////////////////////////////////////////////////////////////////////////
/////////// University of Hawaii, College of Engineering
/////////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022
///////////
/////////// @file catDatabase.h
/////////// @version 1.0 - Initial version
///////////
/////////// This module defines all the enums, arrays, maximum sizes of the array
/////////// and only data.. no functions!
///////////
/////////// @author Christian Li <lichrist@hawaii.edu>
/////////// @date   02_Mar_2022
//////////////////////////////////////////////////////////////////////////////////
////
//
#pragma once
#include <stdio.h>
#include <stdbool.h>

#define MAXIMUM_CAT      30
#define MAXIMUM_CHAR_CAT 30


enum Gender { UNKNOWN_GENDER, MALE, FEMALE };
enum Breed { UNKNOWN_BREED , MAINE_COON , MANX , SHORTHAIR, PERSIAN , SPHINX };

extern int numberOfCats;

extern char catList[MAXIMUM_CAT][MAXIMUM_CHAR_CAT];
extern enum Gender listGender[MAXIMUM_CAT];
extern enum Breed listBreed[MAXIMUM_CAT];
extern bool listIsFixed[MAXIMUM_CAT];
extern float listWeight[MAXIMUM_CAT];
