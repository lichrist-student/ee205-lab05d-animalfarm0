///////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022
///////
/////// @file addCats.c
/////// @version 1.0 - Initial version
///////
/////// This module will add cats to the database. The module only has one
/////// function
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   02_Mar_2022
//////////////////////////////////////////////////////////////////////////////////
//
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"
#include "addCats.h"

int addCat(char name[], int gender, int breed, bool isFixed, float weight){

   if(numberOfCats > MAXIMUM_CAT){
      printf("Error: Database is full!\n");
      return 0;
   }
   if(strlen(name) <= 0){
      printf("Error: The cat's name is empty!\n");
      return 0;
   }
   if(strlen(name) > MAXIMUM_CHAR_CAT){
      printf("Error: The cat's name is greater than 30 characters!\n");
      return 0;
   }
   if(weight <= 0){
      printf("Error: Weight must be greater than 0!\n");
      return 0;
   }
   for(int i=0; i<numberOfCats; i++){
      if(strcmp(catList[i], name) == 0){
         printf("Error: Name must be unique!\n");
         return 0;
      }
   }



strcpy(catList[numberOfCats],name);
listGender[numberOfCats] = gender;
listBreed[numberOfCats] = breed;
listIsFixed[numberOfCats] = isFixed;
listWeight[numberOfCats] = weight;

numberOfCats++;
return numberOfCats;
}
