////////////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022
///////
/////// @file updateCats.c
/////// @version 1.0 - Initial version
///////
/////// This module will have three functions: updateCatName, fixCat, updateCatWeight
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   02_Mar_2022
/////////////////////////////////////////////////////////////////////////////////////
//
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "reportCats.h"
#include "catDatabase.h"
#include "addCats.h"
#include "updateCats.h"

int updateCatName(int index, char newName[]){
   if(index < 0){
      printf("Error: No index!\n");
   }
   if(index >= numberOfCats){
      printf("Error: No cats on the index!\n", index);
   }
   if(strlen(newName) == 0 || strlen(newName) > MAXIMUM_CHAR_CAT){
      printf("Error: Cat's name cannot be empty or greater than 30 charactes!\n");
   }
   for(int i=0; i<numberOfCats; i++){
      if(strcmp(newName, catList[i]) == 0){
         printf("Error: Cat must be unique!\n");
      }
   }
   strcpy(catList[index] , newName);
   printf("The cat at index %d has the name chamged to [%s]\n",index, catList[index]);
   return 0;
}

int fixCat(int index){
   if(index < 0){
      printf("Error: No index!\n");
   }
   if(index >= numberOfCats){
      printf("Error: No cats on the index!\n", index);
   }
   listIsFixed[index] == true;
   return printf("[%s] is fixed\n",catList[index]);
}

int updateCatWeight(int index, float newWeight){
   if(index < 0){
      printf("Error: No index!\n");
   }
   if(index >= numberOfCats){
      printf("Error: No cats on the index!\n", index);
   }
   if(newWeight <= 0){
      printf("Error: Weight must be greater than 0!\n");
   }
   listWeight[index] = newWeight;
   printf("[%s] has been changed to weight: [%f]\n", catList[index], listWeight[index]);
}

