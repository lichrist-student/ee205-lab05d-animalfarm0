////////////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022////////
/////// @file updateCats.h
/////// @version 1.0 - Initial version
///////
/////// This module will have three functions: updateCatName, fixCat, updateCatWeight
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   02_Mar_2022
///////////////////////////////////////////////////////////////////////////////////////
//
#pragma once

extern int updateCatName(int index, char newName[]);
extern int fixCat(int index);
extern int updateCatWeight(int index, float newWeight);
