///////////////////////////////////////////////////////////////////////////////
///////// University of Hawaii, College of Engineering
///////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022
/////////
///////// @file catDatabase.c
///////// @version 1.0 - Initial version
/////////
///////// This module defines all the enums, arrays, maximum sizes of the array
///////// and only data.. no functions!
/////////
///////// @author Christian Li <lichrist@hawaii.edu>
///////// @date   02_Mar_2022
////////////////////////////////////////////////////////////////////////////////
//

#include <stdio.h>
#include <stdbool.h>
#include "catDatabase.h"

#define MAXIMUM_CAT      30
#define MAXIMUM_CHAR_CAT 30


int numberOfCats = 0;

char catList[MAXIMUM_CAT][MAXIMUM_CHAR_CAT];
enum Gender listGender[MAXIMUM_CAT];
enum Breed listBreed[MAXIMUM_CAT];
bool listIsFixed[MAXIMUM_CAT];
float listWeight[MAXIMUM_CAT];

