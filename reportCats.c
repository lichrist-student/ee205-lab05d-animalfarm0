///////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - animal Farm 1 - EE 205 - Spr 2022
///////
/////// @file reportCats.c
/////// @version 1.0 - Initial version
///////
/////// This module will have three functions: printCat, printAllCats, findCat
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   02_Mar_2022
////////////////////////////////////////////////////////////////////////////////////
//
#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"

void printCat(int index){
   if(index < 0){
      printf("animalFarm0: Bad cat [%d]\n", index);
      return 1;
   }
   if(index >= numberOfCats){
      printf("cat index = [%d] name = [%s] gender = [%d] breed = [%d] isFixed = [%d] weight[%f]\n",index, catList[index], listGender[index], listBreed[index], listIsFixed[index], listWeight[index]);
   }
}

void printAllCats(){
   if(numberOfCats == 0){
      printf("No cats in database!\n");
   }
   for(int i=0; i < numberOfCats; i++){
     // printCat(i);
     printf("cat index = [%d] name = [%s] gender = [%d] breed = [%d] isFixed = [%d] weight[%f]\n",i, catList[i], listGender[i], listBreed[i], listIsFixed[i], listWeight[i]);
   }
}

int findCat(char name[]){
   if(strlen(name) <= 0 || strlen(name) > MAXIMUM_CHAR_CAT){
      printf("Error: Cat is either not there or character length is greather than 30!\n");
      return 0;
   }
   for(int i=0; i < numberOfCats; i++){
      if(strcmp(catList[i],name) == 0){
         printf("Index of cat: %d\n",i);
         return i;
      }
   }
   printf("[%s] was not found\n", name);
}
